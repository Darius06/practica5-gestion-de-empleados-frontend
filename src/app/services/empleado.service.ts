import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Empleado } from 'src/app/models/Empleado';


@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  URL_API = 'http://localhost:3000/api/empleados';

  empleados: Empleado[] = [];
  selectedEmpleado: Empleado ={
    nombre:'',
    cargo:'',
    departamento:'',
    sueldo: 0
  }

  constructor(private http: HttpClient) { }

  getEmpleados(){
    const httpOptions = {
      headers: new HttpHeaders({
        'Cache-Control': 'no-cache'
      })
    };
    return this.http.get<Empleado[]>(this.URL_API, httpOptions);
  }

  createEmpleado(empleado:Empleado){
    console.log("empleado ", empleado.sueldo);
    return this.http.post(this.URL_API,empleado);
  }

  deleteEmpleado(id: string) {
    return this.http.delete(`${this.URL_API}/${id}`);
  }
  
  updateEmpleado(empleado: Empleado) {
    console.log("emplreado ",empleado);
    return this.http.put(`${this.URL_API}/${empleado._id}`, empleado);    
  }
  
  
}
