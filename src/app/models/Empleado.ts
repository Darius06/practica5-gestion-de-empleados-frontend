export interface Empleado{
    nombre: string;
    cargo: string;
    departamento: string;
    sueldo: Number;
    createAt?: string;
    updateAt?: string;
    _id?: string;

}