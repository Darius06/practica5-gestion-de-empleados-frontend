import { Component, OnInit } from '@angular/core';
import { EmpleadoService } from 'src/app/services/empleado.service';
import { Empleado } from 'src/app/models/Empleado';
import { FormGroup, NgForm } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import Swal from 'sweetalert2';
// import { ModalService } from 'src/app/services/modal.service';


@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.scss']
})
export class EmpleadoComponent implements OnInit{

  constructor(public empleadoService:EmpleadoService){ }
    empleado: Empleado[]=[];
    nombreFiltro: string = '';
    cargoFiltro: string= '';
    sueldoFiltro: number | null = null;
    departamentoFiltro: string = '';

    ngOnInit(): void {
      this.getEmpleados();
      
    }
 
    getEmpleados(){
      this.empleadoService.getEmpleados().subscribe(
        res=>{
          this.empleadoService.empleados=res;
        },
        err=>console.error(err)
      );
    }

    addEmpleado(form: NgForm){


    if (form.valid) {
    this.empleadoService.createEmpleado(form.value).subscribe(
      res => {
        this.getEmpleados();
        form.reset();
        Swal.fire({
          icon: 'success',
          text: 'Empleado creado correctamente'
        });
      },
      err => console.error(err)
    );
  } else {
    Swal.fire({
      icon: 'error',
      text: 'Por favor, complete todos los campos obligatorios.'
    });
  }
    }
    
    deleteEmpleado(id: string) {      
      this.empleadoService.deleteEmpleado(id).subscribe(
        () => {
          this.getEmpleados();
          Swal.fire({
            icon: 'success',
            text: 'Empleado eliminado correctamente'
          });
        },        
        (err) => console.error(err)        
      );     
    }

    editEmpleado(id: string) {
      const empleado = this.empleadoService.empleados.find(e => e._id === id);
      if (empleado) {
        this.empleadoService.selectedEmpleado = { ...empleado };
       
      } else {
        console.error('Empleado no encontrado');
      }
    }

    updateEmpleado() {
      console.log("ingreso");
      this.empleadoService.updateEmpleado(this.empleadoService.selectedEmpleado).subscribe(
        () => {
          this.getEmpleados();
          Swal.fire({
            icon: 'success',
            text: 'Se actualizo el empleado correctamente'
          });
          this.empleadoService.selectedEmpleado = {
            nombre: '',
            cargo: '',
            departamento: '',
            sueldo: 0
          };
        },
        (err) => console.error(err)
      );
    }
    
    
    
    
}
